const Sequelize = require('sequelize');
const sequelize = require('../db.js');

const Peer = sequelize.define('Peer', {
    address: { type: Sequelize.STRING },
    port: { type: Sequelize.INTEGER },
    publicKey: { type: Sequelize.STRING }
})

Peer.sync();

module.exports = Peer;