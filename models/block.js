const Sequelize = require('sequelize');
const sequelize = require('../db.js');

const Block = sequelize.define('Block', {
    index: { type: Sequelize.INTEGER },
    previousHash: { type: Sequelize.STRING },
    time: { type: Sequelize.DATE },
    data: { type: Sequelize.JSON },
    hash: { type: Sequelize.STRING },
})

Block.sync();

module.exports = Block;