const Sequelize = require('sequelize');
const config = require('./config');
const sequelize = new Sequelize('database', '', '', {
    dialect: 'sqlite',
    storage: './db.sqlite',
    logging: false
  });

module.exports = sequelize;
