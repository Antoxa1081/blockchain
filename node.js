const Koa = require('koa');
const app = new Koa();
const router = require('./router');
const koaBody = require('koa-body');
const utils = require('./utils');
const p2p = require('./router/p2p');
const httpPort = process.argv[3] || 8080;

utils.init();
p2p.initP2PServer();

app.use(koaBody());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(httpPort);
console.log(`listening on ${httpPort}...`);