const fs = require('fs');
const sqlite = require('sqlite3');
const crypto = require('crypto');

const utils = {
    privateKey: '',
    publicKey: '',
    init: function(){
        if(!fs.existsSync('./keys/privateKey.txt') && !fs.existsSync('./keys/publicKey.txt')){
            const { publicKey, privateKey } = crypto.generateKeyPairSync('rsa', {
                modulusLength: 1024,
                publicKeyEncoding: {
                    type: 'spki',
                    format: 'pem'
                },
                privateKeyEncoding: {
                    type: 'pkcs8',
                    format: 'pem',
                }
            });
            fs.writeFileSync('./keys/publicKey.txt', publicKey);
            fs.writeFileSync('./keys/privateKey.txt', privateKey);
            this.publicKey = publicKey;
            this.privateKey = privateKey;
        }
        else {
            this.publicKey = fs.readFileSync('./keys/publicKey.txt').toString();
            this.privateKey = fs.readFileSync('./keys/privateKey.txt').toString();
        }
    },
    isFieldValid: function(string){
        //var letters = /^[A-Za-z]+$/;
        if(string.length >= 4 && string.length <= 32 /*&& string.match(letters)*/){
            return true;
        }
        return false;
    },
    sha256: function(string){
        return crypto.createHash('sha256').update(string).digest('hex');
    },
    privateEncrypt: function(string){
        return crypto.privateEncrypt({ key: this.privateKey }, Buffer.from(string)).toString('base64');
    },
    publicDecrypt: function(string){
        return crypto.publicDecrypt({ key: this.publicKey }, Buffer.from(string, 'base64')).toString();
    },
    parseAddress: function(string){
        return string.slice(string.lastIndexOf(':') + 1);
    }
};

module.exports = utils;