const sequelize = require('../../db.js');
const Block = require('../../models/block');
const Peer = require('../../models/peer');

const blockController = {
    blocks: async function(ctx, next){
        ctx.body = await Block.findAll();
    },
    addBlock: async function(ctx, next){
        
    },
    block: async function(ctx, next){
        var block = await Block.findOne({ where: { index: ctx.params.index }});
        ctx.body = block;
    },
}

module.exports = blockController;