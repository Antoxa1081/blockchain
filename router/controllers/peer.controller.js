const sequelize = require('../../db.js');
const Peer = require('../../models/peer');
const p2p = require('../p2p');

const peersController = {
    peers: async function(ctx, next){
        ctx.body = await Peer.findAll();
    },
    addPeer: async function(ctx, next){
        p2p.addPeer(ctx.request.body.address, ctx.request.body.port);
        ctx.body = {
            success: 'ok'
        }
    }
}

module.exports = peersController;