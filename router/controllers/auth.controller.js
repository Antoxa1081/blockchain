const sequelize = require('../../db.js');
const config = require('../../config.js');

const authController = {
    checkUserToken: async function(ctx, next) {
        if(ctx.request.header.token == config.adminToken){
            await next();
        }
        else
        ctx.body = {
            error: "Invalid token"
        }
    },
}

module.exports = authController;