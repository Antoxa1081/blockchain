const WebSocket = require('ws');
const Peer = require('../models/peer');
const utils = require('../utils');
const url = require('url');
const eventBus = require('./events');

sockets = [];

function initConnection(ws, req){
    if(req === undefined) console.log(`connected to ws://${ws.nodeInfo.address}:${ws.nodeInfo.port}/`)
    else {
        ws.nodeInfo = {};
        ws.nodeInfo.address = utils.parseAddress(req.connection.remoteAddress);
        console.log(`${ws.nodeInfo.address} connected`);
    }
    sockets.push(ws);
    initMessageHandler(ws);
    initErrorHandler(ws);
    sendNodeInfo(ws);
};

function initMessageHandler(ws){
    ws.on('message', (message) => {
        var message = JSON.parse(message);
        eventBus.emit(message.type, message.payload, ws);
    });
};

function initErrorHandler(ws){
    var closeConnection = (ws) => {
        console.log(`connection failed to peer: ws://${ws.nodeInfo.address}:${ws.nodeInfo.port}/`);
        sockets.splice(sockets.indexOf(ws), 1);
    };
    ws.on('close', () => closeConnection(ws));
    ws.on('error', () => closeConnection(ws));
};

function sendNodeInfo(ws){
    ws.send(JSON.stringify({
        type: 'nodeInfo', 
        payload: {
            port: p2p.p2pPort,
            publicKey: utils.publicKey
        }
    }));
}

function connectToPeer(address, port){
    var ws = new WebSocket(`ws://${address}:${port}/`);
    ws.nodeInfo = {};
    ws.nodeInfo.address = address;
    ws.nodeInfo.port = port;
    ws.on('open', () => initConnection(ws));
    ws.on('error', () => {
        console.log('connection failed')
    });
}

const p2p = {
    p2pPort: process.argv[2] || 8090,
    initP2PServer: function(){
        const server = new WebSocket.Server({port: this.p2pPort});
        server.on('connection', (ws, req, client) => { initConnection(ws, req, client) });
        console.log(`listening p2p on ${this.p2pPort}...`);
    },
    addPeer: function(address, port){
        connectToPeer(address, port);
    }
}

module.exports = p2p;