const EventEmitter = require('events');
const Block = require('../models/block');
const Peer = require('../models/peer');

const eventBus = new EventEmitter();

eventBus.on('nodeInfo', async (payload, ws) => {
    if (ws.nodeInfo.port === undefined) ws.nodeInfo.port = payload.port;
    var peer = await Peer.findOne({ where: { address: ws.nodeInfo.address, port: payload.port } });
    if(peer === null){
        ws.nodeInfo.publicKey = payload.publicKey;
        await Peer.create({
            address: ws.nodeInfo.address,
            port: payload.port,
            publicKey: payload.publicKey
        });
        console.log(`Peer ws://${ws.nodeInfo.address}:${payload.port}/ added to DB.`);
    }
    else if(peer.publicKey != payload.publicKey){
        console.log(`Peer ws://${ws.nodeInfo.address}:${payload.port}/ sent wrong key. Disconnecting...`)
        ws.terminate();
    }
});

module.exports = eventBus;