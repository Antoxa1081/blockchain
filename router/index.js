const Router = require('koa-router');
const router = new Router();
const blockController = require('./controllers/block.controller');
const peerController = require('./controllers/peer.controller');
const authController = require('./controllers/auth.controller');

router.get('/blocks', authController.checkUserToken, blockController.blocks);
router.post('/block', authController.checkUserToken, blockController.addBlock);
router.get('/blocks/:index', authController.checkUserToken, blockController.block);

router.get('/peers', authController.checkUserToken, peerController.peers);
router.post('/peer', authController.checkUserToken, peerController.addPeer);

module.exports = router;